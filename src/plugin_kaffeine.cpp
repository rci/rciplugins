#include <dbus/dbus.h>
#include <sstream>
#include <stdio.h>
#include "plugin_kaffeine.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::Plugin_Base *maker()   ///< Factory
{
    return Rci::Plugin_Kaffeine::maker();
}
void eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
    Rci::Plugin_Kaffeine::eraser(in_Ptr);
}

#ifdef __cplusplus
}
#endif

void Rci::KaffeinePlayerProxy::StatusChange(const ::DBus::Struct< int32_t, int32_t, int32_t, int32_t >& status)
{
  setIsRunning( status );
}

Rci::Plugin_Base *Rci::Plugin_Kaffeine::maker()   ///< Factory
{
  Rci::Plugin_Base *ans = new Rci::Plugin_Kaffeine;
  return ans;
}

void Rci::Plugin_Kaffeine::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}

Rci::Plugin_Kaffeine::Plugin_Kaffeine() : Plugin_Dbus("Kaffeine_Plugin"), _playerProxy(*_connectionPlugin, *this), _televisionProxy(*_connectionPlugin)
{
  bool noError = true;
  std::ostringstream ssMessage;

  try{
    _connectionPlugin->start_service("org.kde.kaffeine", 0);
  }
  catch(DBus::Error error)
  {
    ///@todo handle the error
    // service unknown : name = org.freedesktop.DBus.Error.ServiceUnknown
    // method unknown : org.freedesktop.DBus.Error.UnknownMethod --> also thrown if banshee is not completely started
    ssMessage.str("");
    ssMessage << "error n°" << error.name() << ": " << error.message();
    _logger.logIt(ssMessage, LgCpp::Logger::logERROR);
    noError = false;
  }
  
  if(noError)
  {
    // this loop checks wether kaffeine has started or not.
    bool starting;
    do
    {
      starting = false;
      try{
        _playerProxy.GetStatus();
      }
      catch(DBus::Error error)
      {
        starting = true;
        _logger.logIt("kaffeine Starting", LgCpp::Logger::logINFO);
        sleep(1);
      }
    }while(starting);
  }
  
  addState( "Play", boost::bind(&Plugin_Kaffeine::Play, this, _1, _2, _3, _4) );
  addState( "Stop", boost::bind(&Plugin_Kaffeine::Stop, this, _1, _2, _3, _4) );
  addState( "Pause", boost::bind(&Plugin_Kaffeine::Pause, this, _1, _2, _3, _4) );
  addState( "PlayLastChannel", boost::bind(&Plugin_Kaffeine::PlayLastChannel, this, _1, _2, _3, _4) );
  addState( "Forward", boost::bind(&Plugin_Kaffeine::Forward, this, _1, _2, _3, _4) );
  addState( "Backward", boost::bind(&Plugin_Kaffeine::Backward, this, _1, _2, _3, _4) );
  addState( "ToggleFullScreen", boost::bind(&Plugin_Kaffeine::ToggleFullScreen, this, _1, _2, _3, _4) );
  addState( "ToggleMuted", boost::bind(&Plugin_Kaffeine::ToggleMuted, this, _1, _2, _3, _4) );
  addState( "Digit", boost::bind(&Plugin_Kaffeine::Digit, this, _1, _2, _3, _4) );
}

void Rci::Plugin_Kaffeine::Play( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.Play(); }
void Rci::Plugin_Kaffeine::Stop( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.Stop(); }
void Rci::Plugin_Kaffeine::Pause( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.Pause(); }
void Rci::Plugin_Kaffeine::PlayLastChannel( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _televisionProxy.PlayLastChannel(); }
void Rci::Plugin_Kaffeine::Forward( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.ShortSkipForward(); }
void Rci::Plugin_Kaffeine::Backward( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.ShortSkipBackward(); }
void Rci::Plugin_Kaffeine::ToggleFullScreen( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.ToggleFullScreen(); }
void Rci::Plugin_Kaffeine::ToggleMuted( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
  { _playerProxy.ToggleMuted(); }
void Rci::Plugin_Kaffeine::Digit( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{ 
  if( inArguments.size() > 0 )
  {
    if(inArguments[0].size() == 1 )
    {
      std::istringstream issValue;
      issValue.str(inArguments[0].c_str());
      int digit;
      issValue >> digit;
      _televisionProxy.DigitPressed(digit);
    }
    else
      _logger.logIt("Wrong Argument expecting [0..9]", LgCpp::Logger::logWARNING);
    set_TimeInterval( 1, 0 );
  }
  else
    _logger.logIt("Argument missing", LgCpp::Logger::logWARNING);
}

int Rci::Plugin_Kaffeine::get_Key_Value_Catch(const std::string &inKey, std::string &outValue)
{
    int ans = 0;
    
    if(inKey == "position")
    {
        unsigned int time = getPosition();
        
        unsigned int min = time / 60;
        unsigned int sec = time % 60;
        
        char tmp[10];
        if( min < 1000)
            sprintf(tmp, "%02i:%02i", min, sec);
        else
            sprintf(tmp, "999:%02i", sec);
        
        outValue = tmp;
    }
    
    return ans;
}

/** @return position in the media in s
 *
 */
unsigned int Rci::Plugin_Kaffeine::getPosition()
{
  static timeval clockGetIsRunning = {0,0};
  static timeval intervalGetIsRunning = {1,0};
  if (checkTimer(clockGetIsRunning, intervalGetIsRunning));
  {
    _timePosition = _playerProxy.PositionGet();
    _timePosition /= 1000;
  }
  return _timePosition;
}

void Rci::Plugin_Kaffeine::stop_Function()
{
  _playerProxy.Stop();
}

void Rci::Plugin_Kaffeine::start_Catch()
{
  _playerProxy.Play();
}

int Rci::Plugin_Kaffeine::go_To_State_Catch(const std::string &inState, bool inRepeat,
                                            const CmdArguments & inArguments,
                                            std::string &outBackAction, CmdAnswer &outAnswers)
{
  int ans = 0;
  
  outBackAction = "";
  Rci::Plugin_Base::go_To_State( inState, inRepeat, inArguments, outBackAction, outAnswers);
  
  return ans;
}

