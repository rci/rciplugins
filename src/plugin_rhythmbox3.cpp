#include <dbus/dbus.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "plugin_rhythmbox3.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::Plugin_Base *maker()   ///< Factory
{
    return Rci::Plugin_Rhythmbox3::maker();
}

void eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
    Rci::Plugin_Rhythmbox3::eraser(in_Ptr);
}
#ifdef __cplusplus
}
#endif

Rci::Plugin_Base *Rci::Plugin_Rhythmbox3::maker()   ///< Factory
{
  Rci::Plugin_Base *ans = new Rci::Plugin_Rhythmbox3;
  return ans;
}

void Rci::Plugin_Rhythmbox3::getPlaylists( bool inRepeat, CmdArguments const& inArguments,
                                           std::string *outAction, CmdAnswer *outAnswers )
{
  _playlist->initPlaylistList();
  Rci::MprisTemplate::getPlaylists( inRepeat, inArguments, outAction, outAnswers );
}

void Rci::Plugin_Rhythmbox3::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}

Rci::Plugin_Rhythmbox3::Plugin_Rhythmbox3() : MprisTemplate("Rhythmbox3", "org.gnome.Rhythmbox3","/org/mpris/MediaPlayer2") {;}


