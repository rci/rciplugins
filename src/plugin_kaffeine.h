/** @file plugin_kaffeine.h
 *  @brief This file defines the Kaffeine Plugin.
 *
 *   It will only run correctly if the kaffeine dbus service is declared. 
 */

#ifndef __BIGHCI_PLUGIN_RHYTHMBOX_H__
#define __BIGHCI_PLUGIN_RHYTHMBOX_H__

#include <rciDaemon/plugin_dbus.h>
#include "org.mpris.Player-glue.h"
#include "org.mpris.Television-glue.h"

namespace Rci
{
    Plugin_Base *maker();   ///< Factory

/** This class defines a proxy to communicate with the Kaffeine Player module.
 */
class KaffeinePlayerProxy :
    public org::mpris::Player_proxy,
    public DBus::IntrospectableProxy,
    public DBus::ObjectProxy
{
public:
    KaffeinePlayerProxy(DBus::Connection &inConnection, Plugin_Base& inPlugin ) :
        _songChanged(true), _refPlugin(inPlugin), org::mpris::Player_proxy(),
        DBus::ObjectProxy(inConnection,"/Player","org.kde.kaffeine")
        {;}
    ~KaffeinePlayerProxy() {;}
    void setIsRunning(const ::DBus::Struct< int32_t, int32_t, int32_t, int32_t >& status)
    {
      if( status._1 == 0)
        _refPlugin.setRunning();
      else
        _refPlugin.clearRunning();
    }
    
private:
    std::map< std::string, ::DBus::Variant > _metadata;
    bool _songChanged;
    boost::mutex _pMutex;
    
    Plugin_Base &_refPlugin;
    
private:
    void TrackChange(const std::map< std::string, ::DBus::Variant >& metadata) {;}
    void StatusChange(const ::DBus::Struct< int32_t, int32_t, int32_t, int32_t >& status);
    void CapsChange(const int32_t& capabilities) {;}
};

/** This class defines a proxy to communicate with the Kaffeine Television module.
 *
 */
class KaffeineTelevisionProxy :
    public org::mpris::Television_proxy,
    public DBus::IntrospectableProxy,
    public DBus::ObjectProxy
{
public:
    KaffeineTelevisionProxy(DBus::Connection &inConnection) :
        DBus::ObjectProxy(inConnection,"/Television","org.kde.kaffeine")
        {;}
    ~KaffeineTelevisionProxy() {;}
};

 
/** @brief This class defines a Plugin which communicates with kaffeine using Dbus
 */
class Plugin_Kaffeine : public Plugin_Dbus
{
public:
    Plugin_Kaffeine();      ///< Creates the proxy list used by the plugin
    ~Plugin_Kaffeine(){;}

    static Rci::Plugin_Base *maker();   ///< Factory
    static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

    int get_Key_Value_Catch(const std::string &inKey, std::string &outValue); ///< returns the value associated to the key
    void stop_Function();   ///< Stops the plugin (for example stop playing)
    void start_Catch();     ///< Starts the plugin (for example start playing)

    int go_To_State_Catch(const std::string &inState, bool inRepeat, const CmdArguments & inArguments,
                          std::string &outBackAction, CmdAnswer &outAnswers);
                          ///< set the plugin to a given state
protected:
  virtual void Play( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void Stop( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void Pause( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void PlayLastChannel( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void Forward( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void Backward( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void ToggleFullScreen( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void ToggleMuted( bool, CmdArguments const&, std::string *, CmdAnswer * );
  virtual void Digit( bool, CmdArguments const&, std::string *, CmdAnswer * );
  
private:
    KaffeinePlayerProxy _playerProxy;
    KaffeineTelevisionProxy _televisionProxy;

    unsigned int getPosition();
    
    unsigned int _timePosition; ///< position in the media in seconds.
};
}//namespace Rci

#endif //__BIGHCI_PLUGIN_RHYTHMBOX_H__
