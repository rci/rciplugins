/** @file plugin_totem.h
 *  @brief defines a plugin to control Rhythmbox
 */

#ifndef __BIGHCI_PLUGIN_TOTEM_H__
#define __BIGHCI_PLUGIN_TOTEM_H__

#include <plugin_mpris_template.h>

namespace Rci
{

/** @brief This class defines a Plugin which communicates with rhythmbox using Dbus
 *
 */
class Plugin_Totem : public MprisTemplate
{
public:
    static Rci::Plugin_Base *maker();   ///< Factory
    static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

    Plugin_Totem();
    ~Plugin_Totem(){;}

protected:
  void pause( bool, CmdArguments const&, std::string *, CmdAnswer * );
  void stop_Function();         ///< Stops the plugin (for example stop playing)

};

}//namespace Rci

#endif //__BIGHCI_PLUGIN_RHYTHMBOX_H__
