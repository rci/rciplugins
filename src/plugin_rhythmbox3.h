/** @file plugin_rhythmbox.h
 *  @brief defines a plugin to control Rhythmbox
 */

#ifndef __BIGHCI_PLUGIN_RHYTHMBOX_H__
#define __BIGHCI_PLUGIN_RHYTHMBOX_H__

#include <plugin_mpris_template.h>

namespace Rci
{

/** @brief This class defines a Plugin which communicates with rhythmbox using Dbus
 *
 */
class Plugin_Rhythmbox3 : public MprisTemplate
{
public:
    static Rci::Plugin_Base *maker();   ///< Factory
    static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

    void getPlaylists( bool, CmdArguments const&, std::string *, CmdAnswer * );
    
    Plugin_Rhythmbox3();
    ~Plugin_Rhythmbox3(){;}
};

}//namespace Rci

#endif //__BIGHCI_PLUGIN_RHYTHMBOX_H__
