#include <dbus/dbus.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "plugin_totem.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::Plugin_Base *maker()   ///< Factory
{
    return Rci::Plugin_Totem::maker();
}

void eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
    Rci::Plugin_Totem::eraser(in_Ptr);
}
#ifdef __cplusplus
}
#endif

Rci::Plugin_Base *Rci::Plugin_Totem::maker()   ///< Factory
{
    Rci::Plugin_Base *ans = new Rci::Plugin_Totem;
    return ans;
}

void Rci::Plugin_Totem::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}

void Rci::Plugin_Totem::pause( bool inRepeat, CmdArguments const& inArguments, std::string *outAction, CmdAnswer *outAnswers )
{
  _logger.logIt("PlayPause", LgCpp::Logger::logINFO);
  _player->PlayPause();
}

void Rci::Plugin_Totem::stop_Function()
{
  _player->PlayPause();
}

Rci::Plugin_Totem::Plugin_Totem() : MprisTemplate("Totem", "org.mpris.MediaPlayer2.totem","/org/mpris/MediaPlayer2") {;}


