#include <dbus/dbus.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "plugin_banshee.h"

#ifdef __cplusplus
extern "C" {
#endif
Rci::Plugin_Base *maker()   ///< Factory
{
  return Rci::Plugin_Banshee::maker();
}

void eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  Rci::Plugin_Banshee::eraser(in_Ptr);
}
#ifdef __cplusplus
}
#endif

Rci::Plugin_Base *Rci::Plugin_Banshee::maker()   ///< Factory
{
  Rci::Plugin_Base *ans = new Rci::Plugin_Banshee;
  return ans;
}

void Rci::Plugin_Banshee::eraser(Rci::Plugin_Base *in_Ptr)   ///< Factory
{
  delete in_Ptr;
}

Rci::Plugin_Banshee::Plugin_Banshee() : MprisTemplate("Banshee", "org.bansheeproject.Banshee","/org/mpris/MediaPlayer2") {;}


