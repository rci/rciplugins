/** @file plugin_banshee.h
 *  @brief defines a plugin to control Banshee
 *  The plugin defines here relies on several DBus proxy
 */

#ifndef __BIGHCI_Plugin_Banshee_H__
#define __BIGHCI_Plugin_Banshee_H__

#include <plugin_mpris_template.h>

namespace Rci
{

/** @brief This class defines a Plugin which communicates with banshee using Dbus
 *
 */
class Plugin_Banshee : public MprisTemplate
{
public:
    static Rci::Plugin_Base *maker();   ///< Factory
    static void eraser(Rci::Plugin_Base *in_Ptr);   ///< Factory

    Plugin_Banshee();
    ~Plugin_Banshee(){;}
};

}//namespace Rci

#endif //__BIGHCI_Plugin_Banshee_H__
